FROM debian:jessie

RUN apt-get update && apt-get install -y apt-transport-https wget curl

# PHP 7.1
RUN echo 'deb https://packages.sury.org/php/ jessie main' > /etc/apt/sources.list.d/php7.1.list && \
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

RUN apt-get update && \
    apt-get install -y --force-yes php7.1 && \
    apt-get install -y --force-yes php7.1-cli php7.1-xml php7.1-zip php7.1-curl php7.1-mbstring php7.1-gd && \
    apt-get install -y --force-yes git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer

# Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Setup the Composer installer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
  && php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN apt-get update && apt-get install -y nodejs-legacy npm && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
    
RUN npm install n -g
RUN n stable

# Install Grunt, Bower and Webpack.
RUN npm install -g grunt-cli bower webpack webpack-cli

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
